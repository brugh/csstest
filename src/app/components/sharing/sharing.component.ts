import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sharing',
  templateUrl: './sharing.component.html',
  styleUrls: ['./sharing.component.scss']
})
export class SharingComponent implements OnInit {
  @Input() set title(val: string) {
    this._title = val;
    if (this._href) this.genLinks();
  }
  @Input() set href(val: string) {
    this._href = `https://it2benefit.nl/blog/${val}`;
    if (this.title) this.genLinks();
  }
  _href: string;
  _title: string;
  linkedin: string;
  facebook: string;
  twitter: string;

  constructor() { }

  ngOnInit(): void {

  }

  genLinks() {
    this.linkedin = 'https://www.linkedin.com/sharing/share-offsite/?url=' + this._href +
      '&title=' + encodeURIComponent(this._title);
    this.facebook = 'http://www.facebook.com/sharer.php?u=' + this._href;
    this.twitter = 'http://twitter.com/share?text=' + encodeURIComponent(this._title) + '&url=' + this._href;
  }

}
