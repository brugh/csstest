import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeStyle, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  // _bg: SafeStyle;
  _jpg: SafeUrl;
  _webp: SafeUrl;
  alt: string;

  @Input() headline: string;
  // @Input() set img(val:string) {
  //   this._bg = this.domSanitizer.bypassSecurityTrustStyle('center / cover no-repeat url(' + val + ') ');
  // }
  @Input() set img2(val:string) {
    this._jpg = this.domSanitizer.bypassSecurityTrustUrl(val);
    this._webp = this.domSanitizer.bypassSecurityTrustUrl(val.substring(0, val.length-4) + ".webp");
    this.alt = val.substr(val.lastIndexOf('/') + 1);
  }
  @Input() _img2: string;
  @Input() even = false;
  @Input() cls = "img2";

  constructor(private domSanitizer: DomSanitizer) { }

  ngOnInit() { }
}
