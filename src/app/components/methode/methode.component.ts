import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-methode',
  templateUrl: './methode.component.html',
  styleUrls: ['./methode.component.scss']
})
export class MethodeComponent implements OnInit {
  lines: string[] = [ "proposition.line1", "proposition.line2", "proposition.line3", "proposition.line4"];
  constructor(public trans: TranslateService) { }
  ngOnInit() { }
}
