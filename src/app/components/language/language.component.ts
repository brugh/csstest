import { Component, OnInit, EventEmitter, Output, Optional, Inject, PLATFORM_ID } from '@angular/core';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {
  @Output() langChange = new EventEmitter<any>();
  lang = 'nl';

  constructor(public trans: TranslateService,
    @Optional() @Inject(REQUEST) private request: Request,
    @Inject(PLATFORM_ID) private platformId: any) {
    this.trans.addLangs(['en', 'nl']);
    this.trans.setDefaultLang('nl');
  }

  ngOnInit() {
    const l: string = localStorage.getItem('language');
    if (l) {
      this.lang = l;
    } else {
      this.lang = this.getLang();
      if (this.lang !== 'nl' && this.lang !== 'en') this.lang = 'nl';
    }
    this.trans.use(this.lang);
  }

  setLang(lang) {
    this.lang = lang;
    this.langChange.emit();
    this.trans.use(lang);
    localStorage.setItem('language', lang);
  }

  getLang(): string {
    let lang: string;
    if (isPlatformBrowser(this.platformId)) {
      lang = this.trans.getBrowserLang();
    } else {
      lang = (this.request.headers['accept-language'] || '').substring(0, 2);
    }
    return lang;
  }
}