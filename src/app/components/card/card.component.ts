import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() set title(val) {
    this._title = val.replace('&nbsp;',' '); // stupid fix.. 
  }
  @Input() date: string;
  @Input() set img(val) {
    this._img = this.sanitizer.bypassSecurityTrustStyle('url(' + val + ') ');
  };
  @Input() excerpt: string;
  @Input() set link(val) {
    this._link = val; 
    this.href = val + '/';
  };
  _img:SafeStyle;
  _title: string;
  _link: string;
  href: string;
  
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

}
