import { ContactComponent } from './contact/contact.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FillerComponent } from './filler/filler.component';
import { HeroComponent } from './hero/hero.component';
import { InfoComponent } from './info/info.component';
import { MethodeComponent } from './methode/methode.component';
import { FooterComponent } from './footer/footer.component';
import { CardComponent } from './card/card.component';
import { LanguageComponent } from './language/language.component';
import { SharingComponent } from './sharing/sharing.component';

export {
  ContactComponent,
  FillerComponent,
  HeroComponent,
  InfoComponent,
  MethodeComponent,
  NavbarComponent,
  FooterComponent,
  CardComponent, 
  LanguageComponent, 
  SharingComponent
};

export default [
  ContactComponent,
  FillerComponent,
  HeroComponent,
  InfoComponent,
  MethodeComponent,
  NavbarComponent, 
  FooterComponent,
  CardComponent, 
  LanguageComponent,
  SharingComponent
];
