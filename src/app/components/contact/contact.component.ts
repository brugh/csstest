import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  form: FormGroup;
  thanks = false;

  constructor(private fb: FormBuilder, private db: AngularFireDatabase) {
    this.createForm();
  }

  ngOnInit() { }
  get f() { return this.form.controls; }

  createForm() {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      telnr: ['', [Validators.pattern("[0-9 +-]+"), Validators.minLength(10)]],
      message: ['', Validators.required],
    });
  }

  onSubmit() {
    const {name, email, telnr, message} = this.form.value;
    const date = Date();
    const html = `
      <div>From: ${name}</div>
      <div>Email: <a href="mailto:${email}">${email}</a></div>
      <div>Telefoonnr: <a href="tel:${telnr}">${telnr}</a></div
      <div>Date: ${date}</div>
      <div>Message: ${message}</div>
    `;
    const formRequest = { name, email, telnr, message, date, html };
    this.db.list('/messages').push(formRequest);
    this.form.reset();
    this.thanks = true;
    setTimeout(() => {this.thanks = false; }, 5000);
  }
}
