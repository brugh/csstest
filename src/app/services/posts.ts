
export interface Post {
    date: string,
    slug: string,
    title: { rendered: string }, 
    content: { rendered: string },
    excerpt: { rendered: string }
}