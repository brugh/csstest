import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class CanonicalService {

  constructor(@Inject(DOCUMENT) private dom) { }

  setCanonicalURL(url?: string) {
    let domUrl:string = this.dom.URL;
    const i = this.dom.URL.indexOf('#');
    if (i>0) { domUrl = domUrl.substring(0,i); }
    const canURL = url == undefined ? domUrl : window.location.origin + '/' + url;
    let canonical:HTMLLinkElement = this.dom.head.querySelector('link[rel="canonical"]');
    if (!canonical) {
      canonical = this.dom.createElement('link');
      canonical.setAttribute('rel', 'canonical');
      this.dom.head.appendChild(canonical);
    }
    canonical.setAttribute('href', canURL);
  }

}
