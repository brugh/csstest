import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Post } from 'src/app/services/posts';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  posts: Post[] = [];
  post: Post;
  cors= "https://us-central1-csstest-123.cloudfunctions.net/corsme?url=";
  url="https://public-api.wordpress.com/wp/v2/sites/it2benefit.home.blog/posts?_embed";

  constructor(private http: HttpClient) { }

  public getPosts(): Observable<Array<Post>> {
    return new Observable<Array<Post>>(observer => {
      this.loadPosts().subscribe(
        response => {
          if (response) {
            this.posts = response;
            observer.next(this.posts);
            observer.complete();
          } else {
            observer.error(response);
          }
        },
        error => {
          observer.error(error);
        }
      )
    });
  }

  loadPosts(): Observable<Array<Post>> {
    return this.http.get<Array<Post>>(this.url);
  }

  public getPost(slug: string): Observable<Post> {
    return new Observable<Post>(observer => {
      this.loadPost(slug).subscribe(
        response => {
          if (response) {
            this.post = response[0];
            observer.next(this.post);
            observer.complete();
          } else {
            observer.error(response);
          }
        },
        error => {
          observer.error(error);
        }
      )
    });
  }

  loadPost(slug: string): Observable<Post> {
    let url = this.url + "&slug=" + slug;
    console.log("Slug: " + slug + ", URL: " + url);
    return this.http.get<Post>(url);
  }
}
