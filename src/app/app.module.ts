import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientJsonpModule, HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';

import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import en from '../assets/i18n/en.json';
import nl from '../assets/i18n/nl.json';
import { of } from 'rxjs';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './app.material';
import { SanitizeHtmlPipe } from './services/sanitize-html.pipe';

import { MethodeComponent, HeroComponent, ContactComponent, FillerComponent, NavbarComponent, InfoComponent, FooterComponent, CardComponent, LanguageComponent, SharingComponent } from './components';
import { AboutComponent, MainComponent, BlogsComponent, BlogComponent, VoorwaardenComponent, DienstenComponent } from './pages';
import { ScullyLibModule } from '@scullyio/ng-lib';

// export function HttpLoaderFactory(http: HttpClient) { return new TranslateHttpLoader(http, './assets/i18n/', '.json'); }
@Injectable() export class langLoader implements TranslateLoader {
  constructor(){}
  getTranslation(lang: string) { 
    // console.log(nl);
    return of((lang==='en'?en:nl));
  }
}
export function easing(t: number, b: number, c: number, d: number): number {
  if (t === 0) { return b; }
  if (t === d) { return b + c; }
  if ((t /= d / 2) < 1) { return c / 2 * Math.pow(2, 10 * (t - 1)) + b; }
  return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
}

@NgModule({
  declarations: [
    AppComponent,
    MethodeComponent,
    AboutComponent,
    HeroComponent,
    ContactComponent,
    FillerComponent,
    MainComponent,
    NavbarComponent,
    InfoComponent,
    LanguageComponent,
    FooterComponent,
    BlogsComponent,
    BlogComponent,
    CardComponent,
    SanitizeHtmlPipe,
    VoorwaardenComponent,
    DienstenComponent,
    SharingComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    HttpClientJsonpModule,
    AppRoutingModule,
    MatIconModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    NgxPageScrollCoreModule.forRoot( { 
      scrollOffset:56, 
      easingLogic: easing
    }),
    NgxPageScrollModule,
    TranslateModule.forRoot({ 
      loader: { provide: TranslateLoader, useClass: langLoader }
    }),
    // TranslateModule.forRoot({
    //   loader: {
    //       provide: TranslateLoader,
    //       useFactory: HttpLoaderFactory,
    //       deps: [HttpClient]
    //   }
    // }),
    MaterialModule,
    ScullyLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
