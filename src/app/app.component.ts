import { Component, Optional, Inject, PLATFORM_ID, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';

@Component({
  selector: 'app-root',
  template: `<app-navbar></app-navbar><div class="router-wrapper"><router-outlet></router-outlet></div><app-footer></app-footer>`,
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'IT2Benefit - succesvol door IT';

  constructor(private matIconRegistry: MatIconRegistry) {
    matIconRegistry.registerFontClassAlias ('fa');
  }

  ngOnInit() {
  }
}
