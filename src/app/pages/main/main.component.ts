import { Component, OnInit, Inject } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { CanonicalService } from 'src/app/services/canonical.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  data = {title: 'IT2Benefit - IT architecten voor het middenbedrijf'};

  constructor(private route: ActivatedRoute, private title: Title, private meta: Meta, private canonical: CanonicalService) { }

  ngOnInit() {
    this.title.setTitle(this.data.title);
    this.canonical.setCanonicalURL();
    this.meta.updateTag({ name: 'twitter:card', content: 'IT2Benefit main page' });
    this.meta.updateTag({ name: 'og:url', content: 'main' });
    // if (this.route.snapshot.routeConfig.data) window.location.hash = this.route.snapshot.routeConfig.data.fragment;
  }
}
