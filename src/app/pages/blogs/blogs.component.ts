import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Title, Meta } from '@angular/platform-browser';
import { Post } from '../../services/posts';
import { CanonicalService } from 'src/app/services/canonical.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit { 
  data = {title: 'IT2Benefit - blog posts over architectuur en programmamanagement'};
  posts: Array<Post> = [];

  constructor(private postsvc: PostsService, private title: Title, private meta: Meta, private canonical: CanonicalService) { }

  ngOnInit() {
    this.postsvc.getPosts().subscribe((posts) => {
      this.posts = posts;
      for (let p of this.posts) { 
        p.excerpt.rendered = p.excerpt.rendered.replace(/<p class="link-more".*<\/p>/,'');
      }
      // console.log(JSON.stringify(this.posts.length));
    });
    this.title.setTitle(this.data.title);
    this.canonical.setCanonicalURL('blogs');
    this.meta.updateTag({ name: 'twitter:card', content: 'IT2Benefit blogs' });
    this.meta.updateTag({ name: 'og:url', content: 'blogs' });
  }

}
