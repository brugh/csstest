import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { CanonicalService } from 'src/app/services/canonical.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(public trans: TranslateService, private route: ActivatedRoute, private title: Title, private meta: Meta, private canonical: CanonicalService) { }

  ngOnInit() { 
    console.log(this.route.snapshot);
    this.canonical.setCanonicalURL('about');
    // if (this.route.snapshot.routeConfig.data) window.location.hash = this.route.snapshot.routeConfig.data.fragment;
    this.title.setTitle("IT2Benefit - about");
    this.meta.updateTag({ name: 'twitter:card', content: 'Over IT2Benefit' });
    this.meta.updateTag({ name: 'og:url', content: 'about' });
  }
}
