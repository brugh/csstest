import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title, Meta } from '@angular/platform-browser';
import { Post } from '../../services/posts';
import { CanonicalService } from 'src/app/services/canonical.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BlogComponent implements OnInit {
  data = {title: 'IT2Benefit - blog post over architectuur en programmamanagement'};
  activeRoute: Subscription;
  postSlug: string;
  post: Post;

  constructor(private route: ActivatedRoute, private postsvc: PostsService, private title: Title, private meta: Meta, private canonical: CanonicalService) { }

  ngOnInit() {
    this.postsvc.getPost
    this.activeRoute = this.route.params.subscribe(params => {
      if (typeof params['slug'] != "undefined") {
        this.postSlug = params['slug'];
        console.log(this.postSlug);
        this.postsvc.getPost(this.postSlug).subscribe((post) => {
          this.post = post;
          console.log(this.post.title.rendered);
          this.post.title.rendered = this.post.title.rendered.replace(/&nbsp;/,' ');
          this.data = { title: this.post.title.rendered };
          this.canonical.setCanonicalURL('blog/' + this.postSlug);
          this.settitle(this.data.title);
        });
      }
    });
    this.settitle(this.data.title); 
  }

  settitle(title) {
    this.title.setTitle(title);
    this.meta.updateTag({ name: 'twitter:card', content: 'IT2Benefit blog' });
    this.meta.updateTag({ name: 'og:url', content: 'blog' });
  }
}
