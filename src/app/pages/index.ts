import { AboutComponent } from './about/about.component';
import { MainComponent } from './main/main.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogComponent } from './blog/blog.component';
import { VoorwaardenComponent } from './voorwaarden/voorwaarden.component';
import { DienstenComponent } from './diensten/diensten.component';

export {
  AboutComponent,
  MainComponent,
  BlogsComponent,
  BlogComponent,
  VoorwaardenComponent,
  DienstenComponent
}

export default [
  AboutComponent,
  MainComponent,
  BlogsComponent,
  BlogComponent, 
  VoorwaardenComponent,
  DienstenComponent
]