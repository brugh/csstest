import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent, AboutComponent, BlogsComponent, BlogComponent, VoorwaardenComponent } from './pages';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'about', component: AboutComponent },
  { path: 'blog', component: BlogsComponent },
  { path: 'blog/:slug', component: BlogComponent },
  { path: 'voorwaarden', component: VoorwaardenComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: "enabled",
    onSameUrlNavigation: "reload",
    enableTracing: false,
    initialNavigation: 'enabled'
})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
