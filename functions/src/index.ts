import * as functions from 'firebase-functions';
import { enableProdMode } from  '@angular/core';
enableProdMode();

const universal = require(`${process.cwd()}/dist/server`).app;
export const ssr = functions.https.onRequest(universal);

const cors = require('cors')({ origin: true });
const blockedPhrases = new RegExp(/porn|sexy/);  // No thank you.
const fetch = require('node-fetch');

export const corsme = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    let url = req.query.url;
    if (!url) { url = req.body.url; }
    if (!url) { res.status(403).send('URL is empty.'); }
    console.log('Request:', url);
    if (url.match(blockedPhrases)) { res.status(403).send('Phrase in URL is disallowed.'); }

    fetch(url, {
      method: req.method,
      headers: { 'Content-Type': req.get('Content-Type') }
    })
      .then((r: Response) => r.headers.get('content-type') === 'application/json' ? r.json() : r.text())
      .then((body: string) => res.status(200).send(body));
  });
});

const nodemailer = require('nodemailer');
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
         user: 'servicemailer012@gmail.com',
         pass: 'thisismyservicepassword'
     }
});

exports.sendContactMessage = functions.database.ref('/messages/{pushKey}').onWrite(async (snapshot, context) => {
  console.log(JSON.stringify(snapshot.after));
  const val = snapshot.after.val();
  if (snapshot.before.val() || !snapshot.after.val().name) {
    return;
  }
  const mailOptions = {
    to: 'steven@it2benefit.nl, herco@it2benefit.nl, info@it2benefit.nl',
    subject: `Information Request van de website van ${val.name}`,
    html: val.html
  };
  await mailTransport.sendMail(mailOptions).then(() => {
    console.log('Mail sent to: it2benefit.nl');
  });
});
