#!/usr/bin/bash
set -x
M=$(ls dist/browser/main*.js)
mv $M main.js
sed -e 's#assets#ssr/assets#g' main.js > $M
sed -e 's#src="#src=\"ssr/#g' dist/browser/index.html > dist/browser/index2.html
sed -e 's#href="styles#href="ssr/styles#' dist/browser/index2.html > dist/browser/index.html
